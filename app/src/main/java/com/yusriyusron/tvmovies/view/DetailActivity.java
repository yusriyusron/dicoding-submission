package com.yusriyusron.tvmovies.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.yusriyusron.tvmovies.R;
import com.yusriyusron.tvmovies.model.Movie;
import com.yusriyusron.tvmovies.model.TvShow;

public class DetailActivity extends AppCompatActivity {

    public static String MOVIES = "MOVIES";
    public static String TV_SHOWS = "TV_SHOWS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        ImageView imageView = findViewById(R.id.image_movies);
        TextView textViewTitle = findViewById(R.id.title_movies);
        TextView textViewOverview = findViewById(R.id.overview_movies);
        TextView overview = findViewById(R.id.overview);

        overview.setText(R.string.overview);

        Movie movies = getIntent().getParcelableExtra(MOVIES);
        TvShow tvShows = getIntent().getParcelableExtra(TV_SHOWS);

        if (movies != null){
            Glide.with(this).load(movies.getImageMovie()).override(150,150).into(imageView);
            textViewTitle.setText(movies.getTitleMovie());
            textViewOverview.setText(movies.getOverviewMovie());
        }else {
            Glide.with(this).load(tvShows.getImageTvShow()).override(150,150).into(imageView);
            textViewTitle.setText(tvShows.getTitleTvShow());
            textViewOverview.setText(tvShows.getOverviewTvShow());
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            makeIntent(MainActivity.class);
        }
        return super.onOptionsItemSelected(item);
    }

    private void makeIntent(Class destination){
        Intent intent = new Intent(this,destination);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        makeIntent(MainActivity.class);
    }
}
